// constants for fp2
//
// (C) 2019 anonymous authors. All rights reserved.

#ifndef __bls_hash__src__curve2__consts2_h__

#include <stdint.h>

// constants for SvdW map
extern const uint64_t Icx12[6], IsqrtM3[6], Iinv3[6];

// constants to compute values of eta for SWU map
extern const uint64_t Ieta1[6];
extern const uint64_t Ieta2[6];

#define __bls_hash__src__curve2__consts2_h__
#endif  // __bls_hash__src__curve2__consts2_h__
